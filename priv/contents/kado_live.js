// Kado live-reload
function live() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var resp = JSON.parse(xhttp.responseText);
      if (resp.reload == 'true') {
        document.location.reload();
      }
    }
  };
  xhttp.open("GET", "live", true);
  xhttp.send();
  setTimeout(live, 1000);
}
setTimeout(live, 1000);
