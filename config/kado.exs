use Mix.Config

# This file is responsible for configuring your application
config :kado, [
  name: "kado-starter",
  language: ['en', 'ja'],
  theme: "default",
  url: "https://gitlab.com/samcns/kado-starter",
  port: 3000
]
