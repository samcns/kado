defmodule Kado.Mixfile do
  use Mix.Project

  def project do
    [app: :kado,
     version: "0.1.0",
     elixir: "~> 1.3",
     escript: [main_module: Kado],
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [
     applications: [
        :cowboy, 
        :logger,
        :yaml_elixir,
        :fwatch,
        :html_entities,
        :stache,
        :amnesia
    ]]
  end

  defp deps do
    [
      {:cowboy, "~> 1.0.0"},
      {:yaml_elixir, "~> 1.2.0"},
      {:yamerl, "~> 0.3.2"},
      {:fwatch, "~> 0.1"},
      {:html_entities, "~> 0.3"},
      {:stache, "~> 0.2.1"},
      {:amnesia, github: "meh/amnesia", tag: :master}
    ]
  end
end
