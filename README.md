# Kado

Kado is a static site generator with built-in web server, file modification watcher, live reload, i18n, themes, mustache template support, and multi-page support.

## Installation

1. Get and build deps, compile `kado`

  ```
  mix deps.get
  mix deps.compile
  mix escript.build
  ```

Features
========
* **Easy to use**: Based on existing static site generator conventions
* **Built-in development webserver**: Test your modifications (http://localhost:3000) as you work
* **Auto Re-render**: `kado --watch` monitors the `theme/[your-theme]/layout/`, `pages`, `partials/`, and `i18n/` folders for modifications and auto-renders to build
* **Live reload**: `kado --watch` automatically reloads the browser when a re-render occurs
* **i18n support**: Use YAML to define each language in the `i18n/` folder (e.g. `i18n/en.yml`)
* **Page / layout support**: Generate individual pages wrapped in the same theme layout
* **Trigger rebuild manually**: Press `r enter` to initiate a full rebuild. This is useful for when you add new files or modify files that are not actively monitored by `kado`, e.g. images, css, fonts, or any non .ms or .yml files
* **Actively developed**: More features coming soon (e.g. more tests, AWS, Github Pages, SSH support...)

**Note**: Kado is a work in progress. It is functional and does a bunch of cool stuff, but it isn't perfect. Please post any [issues](https://gitlab.com/samcns/kado/issues) you find.

Usage
=====
```
  Usage:
  ./kado --init
  ./kado --watch
  ./kado --build
  ./kado --serve

  Options:
  --config  config.yml path
  --help    Show this help message.
```

Todo
====

* Add tests
* Uglify JS / CSS
* Build deploy process push to S3
* Features
  * Posts for blogs

Requirements
============

* [Elixir](http://elixir-lang.org/)

AUTHORS
=======

* [Sam Morrison](@samcns)
