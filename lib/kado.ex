defmodule Kado.DB do

  def set_reload_status(status) do
    require Amnesia
    use Amnesia
    use Database
    Amnesia.transaction do
      %Reload{timestamp: 0, status: status} |> Reload.write
    end
  end

  def get_reload_status() do
    use Database
    use Amnesia
    Amnesia.transaction do
      # Return current Reload status
      case Reload.read(0).status do
        true  -> "True"
        false -> "False"
      end
    end
  end

  def initialize_reload_db() do
    # Create Reload DB in ram
    require Amnesia
    use Amnesia
    Amnesia.Schema.create
    Amnesia.start
    Database.create(ram: [node])
    Database.wait

    # Set status = false for inital load
    set_reload_status(false)
  end

end

defmodule Kado.LiveJSHandler do
  def init(_transport, req, []) do
    {:ok, req, nil}
  end

  def handle(req, state) do
    live_js = """
      // Kado live-reload
      function live() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
            var resp = JSON.parse(xhttp.responseText);
            if (resp.reload == 'True') {
              document.location.reload();
            };
          };
        };
        xhttp.open("GET", "live", true);
        xhttp.send();
        setTimeout(live, 1000);
      }
      setTimeout(live, 1000);
      """
    {:ok, req} = :cowboy_req.reply(200, [], live_js, req)
    {:ok, req, state}
  end

  def terminate(_reason, _req, _state), do: :ok
end

defmodule Kado.LiveHandler do
  def init(_transport, req, []) do
    {:ok, req, nil}
  end

  def handle(req, state) do
    # Get current Reload status
    status = Kado.DB.get_reload_status
    # Reset Reload status to false if True
    if status == "True", do: Kado.DB.set_reload_status(false)
    {:ok, req} = :cowboy_req.reply(200, [], "{ \"reload\" : \"#{status}\" }", req)
    {:ok, req, state}
  end

  def terminate(_reason, _req, _state), do: :ok
end

defmodule Kado.ReloadHandler do
  def init(_transport, req, []) do
    {:ok, req, nil}
  end

  def handle(req, state) do
    {:ok, req} = :cowboy_req.reply(200, [], "{ 'reload' : 'staged' }", req)
    {:ok, req, state}
  end

  def terminate(_reason, _req, _state), do: :ok
end

defmodule Kado do

  def web_server(config) do
    dispatch = :cowboy_router.compile([
      {:_, 
        [
          {"/", :cowboy_static, {:file, "build/index.html"}},
          {"/kado_live.js", Kado.LiveJSHandler, []},
          {"/live", Kado.LiveHandler, []},
          {"/reload", Kado.ReloadHandler, []},
          {"/[...]", :cowboy_static, {:dir, "build"}}
      ]}
    ])
    :cowboy.start_http(:http,
                        100,
                        [{ :port, 3000 }],
                        [{ :env, [{:dispatch, dispatch}] }])
    web_server(config)
  end

  def serve(config) do
    # Start web server
    config |> web_server
  end

  def templates(exts, dir) do
    Enum.map(File.ls!(dir), fn file ->
      unless is_nil(file) do
        if Path.extname(file) == ".ms" do
          "#{dir}/#{file}"
        end
      end
    end) |> Enum.reject(fn n -> n == nil end)
  end

  def load_pages(pages) do
    Enum.map(pages, fn page -> 
      unless is_nil(page) do
        body = File.read!(page)
        page_name = "#{Path.basename(page, ".ms")}"
        unless page_name == "" do
          %{"file_name": page, "body": body}
        end
      end
    end)
  end

  def load_partials(partials) do 
    Enum.reduce partials, %{}, fn partial, acc -> 
      unless is_nil(partial) do
        body = File.read!(partial)
        partial_name = "#{Path.basename(partial, ".ms")}"
        partial_map = %{"#{partial_name}": "#{body}"} 
        Map.merge(acc, partial_map)
      end
    end
  end

  def build_context(i18n_dir, language) do
    i18n_file = "#{i18n_dir}/#{language}.yml";
    if File.exists?(i18n_file) do
      context = YamlElixir.read_from_file i18n_file

      # Cleanup the context Map
      new_context = Enum.reduce context, %{}, fn item, acc ->
        {key, value} = item
        Map.merge(acc, %{"#{key}": "#{value}"})
      end

      # Add i18n variables
      context_plus = %{
        "language": language,
        "#{language}": true
      }

      # Merge Maps for output
      Map.merge(new_context, context_plus)
    end
  end

  def append_live_js_script(string) do
    String.replace(string,
                   "</body>",
                   "<script src=\"kado_live.js\"></script></body>")
  end

  def prepare_html_output(context, 
                          template_dirs,
                          default_language,
                          language,
                          theme,
                          pages,
                          partials,
                          no_livereload) do

      Enum.map pages, fn page ->
        unless is_nil(page) do
          page_content = page.body
                         |> Stache.eval_string(context, partials)
                         |> HtmlEntities.decode

          new_context = Map.merge(%{"content": page_content}, context)
          {:ok, layout_body}= File.read("themes/#{theme}/layout/layout.ms")

          layout_content = layout_body
                         |> Stache.eval_string(new_context, partials)
                         |> HtmlEntities.decode

          page_basename = Path.basename(page.file_name, ".ms")
          page_name = case language == default_language do
            true  -> page_basename
            false -> "#{page_basename}-#{language}"
          end

          case no_livereload do
            # Disable livereload for build
            true  -> %{"page_name": page_name, "body": layout_content}
            # Enable livereload for watch
            false -> %{"page_name": page_name,
                       "body": layout_content |> append_live_js_script }
          end
        end
      end
  end

  def write_generated_files(content, build_dir) do
    # IO write to disk
    Enum.each content, fn html ->
      unless is_nil(html) do
        File.write "#{build_dir}/#{html.page_name}.html", html.body
      end
    end
  end

  def render(config, no_livereload) do
    "Begin render..." |> IO.puts

    # Basics
    exts             = config.extensions
    theme            = config.theme
    build_dir        = config.build_dir
    assets_dir       = config.assets_dir
    i18n_dir         = config.i18n_dir
    template_dirs    = config.template_dirs

    # Process templates
    pages_dir        = config.pages_dir
    pages            = templates(exts, pages_dir) |> load_pages
    partials_dir     = config.partials_dir
    partials         = templates(exts, partials_dir) |> load_partials

    # Get i18n ready
    languages        = Map.get(config, "language")
    default_language = languages |> List.first

    # Empty build directory
    File.rm_rf(build_dir)
    File.mkdir(build_dir)

    # Copy assets
    File.cp_r!("#{assets_dir}", "#{build_dir}")

    Enum.each(languages, fn language ->
      build_context(i18n_dir, language)
      |> prepare_html_output(template_dirs, 
                             default_language,
                             language,
                             theme,
                             pages,
                             partials,
                             no_livereload)
      |> write_generated_files(build_dir)
    end)

    "Render complete" |> IO.puts
  end

  def file_watcher(parent, paths, extensions) do
    # Watch multiple directories and registers a callback
    Fwatch.watch_dir(paths, fn path, events ->
      if :modified in events and Path.extname(path) in extensions do
        send parent, { :modified, "#{path} is modified" }
      end
    end)
  end

  def file_watch_supervisor(config, parent) do
    receive do
      {:modified, response} ->
        render(config, false)
        Kado.DB.set_reload_status(true)
    end
    file_watch_supervisor(config, parent)
  end

  def keybinding(config) do
    key = IO.gets("Press [r + enter] to trigger rebuild\n")
    if String.trim_trailing(key) == "r" do
      config |> render(false)
      Kado.DB.set_reload_status(true)
    end
    keybinding(config)
  end

  def watch(config) do
    # Start Reload DB
    Kado.DB.initialize_reload_db()

    # Start file watch supervisor
    fws_pid = spawn(__MODULE__, :file_watch_supervisor, [config, self()])

    # Start file watcher
    spawn(__MODULE__, :file_watcher, [fws_pid, config.template_dirs, config.extensions])

    # Initial render
    config |> render(false)
    # Start web server
    "Serving files at... " |> IO.puts
    spawn(__MODULE__, :serve, [config])

    # Listen for user input
    keybinding(config)
  end

  def load_config() do
    path = File.cwd! |> Path.join("config.yml")
    case File.exists?(path) do
      true  -> parse_config path
      false -> { :error, "File not found #{path}" }
    end
  end

  def load_config(path) do
    case File.exists?(path) do
      true  -> parse_config path
      false -> { :error, "File not found #{path}" }
    end
  end

  def expand_home_path(path) do
    String.replace( path, "~", System.get_env("HOME") )
  end

  def parse_config(path) do

    config = YamlElixir.read_from_file path

    project_root = ( Map.get(config, "project_root") || System.cwd() ) |> expand_home_path
    build_dir    = "#{project_root}/build"
    theme        = "#{Map.get(config, "theme") || "default"}"
    theme_dir    = "#{project_root}/themes/#{Map.get(config, "theme") || "default"}"
    layout_dir   = "#{theme_dir}/layout"
    assets_dir   = "#{theme_dir}/assets"
    partials_dir = "#{project_root}/partials"
    pages_dir    = "#{project_root}/pages"
    i18n_dir     = "#{project_root}/i18n"

    config_plus  = %{
      "logger":             "logger",
      "host":               Map.get(config, "host") || "0.0.0.0",
      "port":               Map.get(config, "port") || 3000,
      "project_root":       project_root,
      "path":               path,
      "build_dir":          build_dir,
      "theme":              theme,
      "themes_dir":         theme_dir,
      "assets_dir":         assets_dir,
      "layout_dir":         layout_dir,
      "pages_dir":          pages_dir,
      "partials_dir":       partials_dir,
      "i18n_dir":           i18n_dir,
      "template_dirs":      [layout_dir, partials_dir, pages_dir, i18n_dir],
      "extensions":         [".ms", ".tt", ".html", ".yml"]
    }

    Map.merge(config, config_plus)

  end

  def print_config(config) do
    Enum.map(config, fn {k, v} ->  
      case is_list(v) do
        true ->
          # Print out lists
          "#{k}:\n#{Enum.map(v, fn c -> "  #{c}\n" end )}\n"
        _ ->
          # Print k/v pair
          "#{k}: #{v}\n\n"
      end
    end) |> IO.puts
  end

  def do_process(:init) do
    #Kado.Config.load() |> IO.puts
    IO.puts "Creating project..."
  end

  def do_process(:watch) do
    load_config |> watch
  end

  def do_process([:watch, config_file]) do
    load_config("#{config_file}") |> watch
  end

  def do_process(:build) do
    IO.puts "Building files..."
    load_config |> render(true) 
  end

  def do_process([:build, config_file]) do
    IO.puts "Building files with config..."
    load_config("#{config_file}") |> render(true)
  end

  def do_process(:serve) do
    load_config |> serve
  end

  def do_process([:serve, config_file]) do
    load_config("#{config_file}") |> serve
  end

  def do_process(:help) do
    IO.puts """
      Usage:
      ./kado --init
      ./kado --watch
      ./kado --build
      ./kado --serve

      Options:
      --config  config.yml path
      --help    Show this help message.
    """

    System.halt(0)
  end

  def parse_args(args) do
    options = OptionParser.parse(args)
    case options do
      {[name: name], _, _}  -> [name]
      {[init: true], _, _}  -> :init
      {[watch: true], _, _} -> :watch
      {[watch: true, config: config_file], _, _} -> [:watch, config_file]
      {[build: true], _, _} -> :build
      {[build: true, config: config_file], _, _} -> [:build, config_file]
      {[serve: true], _, _} -> :serve
      {[serve: true, config: config_file], _, _} -> [:serve, config_file]
      {[help: true], _, _}  -> :help
      _ -> :help
    end
  end

  def main(args) do
    args |> parse_args |> do_process
  end

end
