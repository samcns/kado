require Amnesia
use Amnesia

defdatabase Database do
  deftable Reload, [:timestamp, :status], type: :ordered_set do 
    @type t :: %Reload{timestamp: non_neg_integer, status: Bool.t}
  end
end
